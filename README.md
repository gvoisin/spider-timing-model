# Spider_Timing_model

## What ?
 This is a version of ELL1model.C of Tempo2 2018 modified to implement the Spider timing model ( "A spider timing model: accounting for quadrupole deformations and relativity in close pulsar binaries, Voisin et al. 2019)

 This file is the version used for the paper "First measurement of the gravitational quadrupole moment of a blackwidow companion in PSR J2051-0827", Voisin et al., MNRAS, 2019 (DOI: 10.1093/mnras/staa953, HAL: https://hal.archives-ouvertes.fr/hal-02427662, Arxiv : https://arxiv.org/abs/2004.01564)


## How to use it :
Replace the ELL1model.C file in the Tempo2 sources by this one, and recompile. 

The ELL1 model will then work as before, except that any OMDOT entry in the parfile will now be taken into account. 

